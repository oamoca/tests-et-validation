﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace InterfaceImmobilier
{
	public static class ToolBox
	{
		public static void ControlText(TextCompositionEventArgs e)
		{
			if (!char.IsDigit(e.Text, e.Text.Length - 1))
				e.Handled = true;

			if (e.Text == ".")
				e.Handled = false;
		}

		public static int ConvertStringToInt(string str)
		{
			if (Int32.TryParse(str, out int value))
			{
				return value;
			}
			return value;
		}

		public static double ConvertStringToDouble(string str)
		{
			if (Double.TryParse(str, out double value))
			{
				return value;
			}
			return value;
		}

		public static bool ControlMontant(string montant)
		{
			bool result = true;
			if (montant != "")
			{
				if (ConvertStringToInt(montant) < 50000)
				{
					MessageBox.Show("Montant ne peux pas être inférieur à 50 000 €", "Error montant", MessageBoxButton.OK, MessageBoxImage.Error);
					result = false;
				}
			}

			if (montant == "")
			{
				MessageBox.Show("Montant ne peux pas être null", "Error montant", MessageBoxButton.OK, MessageBoxImage.Error);
				result = false;
			}

			return result;
		}

		public static bool ControlDuree(string duree)
		{
			bool result = true;
			if (duree != "")
			{
				if (ConvertStringToInt(duree) < 9 || ConvertStringToInt(duree) > 25)
				{
					MessageBox.Show("La durée doit être comprise entre 9 et 25 ans", "Error durée", MessageBoxButton.OK, MessageBoxImage.Error);
					result = false;
				}
			}

			if (duree == "")
			{
				MessageBox.Show("La durée ne peux pas être null", "Error durée", MessageBoxButton.OK, MessageBoxImage.Error);
				result = false;
			}

			return result;
		}

		public static bool ControlTaux(string typeTaux, string dureeEmprunt)
		{
			bool result = true;

			if (typeTaux == "" && dureeEmprunt == "")
			{
				MessageBox.Show("Le taux n'est pas renseigné", "Error taux", MessageBoxButton.OK, MessageBoxImage.Error);
				result = false;
			}

			return result;
		}
	}
}
