﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Immobilier
{
	public class Credit
	{
		private double Montant { get; set; }
		private double Taux { get; set; }
		private int Duree { get; set; }

		public Credit(double montant, int duree, double taux)
		{
			this.Montant = montant;
			this.Duree = duree;
			this.Taux = taux;
		}

		public double GetMensualite()
		{
			double mensualite = 0;
			double taux = Taux / 100;
			double tauxMensuel = taux / 12;
			double nbMois = Duree * 12;
			mensualite = Montant * tauxMensuel / (1 - Math.Pow(1 + tauxMensuel, -nbMois));
			return mensualite;
		}
	}
}
