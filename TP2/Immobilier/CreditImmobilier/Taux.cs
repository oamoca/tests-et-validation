﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
	public class Taux
	{
		private double _tauxActuel { get; set; }
		private int _duree { get; set; }
		private int _tauxStatusActuel { get; set; }
		private enum TauxStatus
		{
			BonTaux = 0,
			TresBonTaux = 1,
			ExcellentTaux= 2
		};

		Dictionary<int, double> BonTaux = new Dictionary<int, double>()
		{
			{ 7, 2.56 },
			{ 10, 2.63 },
			{ 15, 2.79 },
			{ 20, 2.90 },
			{ 25, 3.00 }
		};

		Dictionary<int, double> TresBonTaux = new Dictionary<int, double>()
		{
			{ 7, 2.37 },
			{ 10, 2.47 },
			{ 15, 2.67 },
			{ 20, 2.76 },
			{ 25, 2.88 }
		};

		Dictionary<int, double> ExcellentTaux = new Dictionary<int, double>()
		{
			{ 7, 2.10 },
			{ 10, 2.15 },
			{ 15, 2.33 },
			{ 20, 2.33 },
			{ 25, 2.45 }
		};


		public Taux(int tauxStatus, int duree)
		{
			this._tauxStatusActuel = tauxStatus;
			this._duree = duree;
			init();
		}

		public void init()
		{
			Dictionary<int, double> TauxChoisis = new Dictionary<int, double>();

			switch (this._tauxStatusActuel)
			{
				case (int)TauxStatus.BonTaux:
					TauxChoisis = BonTaux;
					break;
				case (int)TauxStatus.TresBonTaux:
					TauxChoisis = TresBonTaux;
					break;
				case (int)TauxStatus.ExcellentTaux:
					TauxChoisis = ExcellentTaux;
					break;
			}

			this._tauxActuel = TauxChoisis[this._duree];
		}

		public double GetTaux { get { return this._tauxActuel; } }
	}
}
