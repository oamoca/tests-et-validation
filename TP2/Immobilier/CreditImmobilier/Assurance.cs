﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
	public class Assurance
	{
		private double _tauxActuel;

		private const double _tauxBase = 0.003;
		private const double _tauxSportif = -0.0005;
		private const double _tauxFumeur = 0.0015;
		private const double _tauxTroublesCardiaques = 0.003;
		private const double _tauxIngenieurInformatique = -0.0005;
		private const double _tauxPiloteDeChasse = 0.0015;

		public Assurance()
		{
			init();
		}

		private void init()
		{
			this._tauxActuel += _tauxBase;
		}

		public void SetSportif(bool? status)
		{
			if ((bool)status)
				this._tauxActuel += _tauxSportif;
		}

		public void SetFumeur(bool? status)
		{
			if ((bool)status)
				this._tauxActuel += _tauxFumeur;
		}

		public void SetTroublesCardiaques(bool? status)
		{
			if ((bool)status)
				this._tauxActuel += _tauxTroublesCardiaques;
		}

		public void SetIngenieurInformatique(bool? status)
		{
			if ((bool)status)
				this._tauxActuel += _tauxIngenieurInformatique;
		}

		public void SetPiloteDeChasse(bool? status)
		{
			if ((bool)status)
				this._tauxActuel += _tauxPiloteDeChasse;
		}

		public double GetTauxAssurance()
		{
			return this._tauxActuel;
		}
	}
}
