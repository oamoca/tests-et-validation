using CreditImmobilier;
using Xunit;

namespace TestImmobilier
{
	public class TestImmobilier
	{
		[Theory]
		[InlineData(60000, 24, 0, 10, 281.18)]
		[InlineData(80000, 9, 1, 20, 837.39)]
		[InlineData(175000, 25, 0, 25, 829.87)]
		public void CalculMensualite(double montant, int duree, int tauxStatus, int tauxDuree, double expectedValue)
		{
			//Avverage
			Credit immobilier = new Credit(montant, duree, tauxStatus, tauxDuree);

			//Act
			double actualValue = immobilier.GetMensualite();

			//Assert
			Assert.Equal(expectedValue, actualValue, 2);
		}

		[Fact]
		public void TauxAssurance()
		{
			Assurance assurance = new Assurance();
			assurance.SetSportif(true);
			assurance.SetFumeur(true);
			assurance.SetTroublesCardiaques(true);
			assurance.SetIngenieurInformatique(true);
			assurance.SetPiloteDeChasse(true);

			double expectedValue = 0.008;
			double actualValue = assurance.GetTauxAssurance();

			Assert.Equal(expectedValue, actualValue, 4);
		}
	}
}