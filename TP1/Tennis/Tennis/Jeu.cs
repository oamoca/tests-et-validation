﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public class Jeu
    {
		private Joueur[] joueurs = new Joueur[] { new Joueur(1), new Joueur(2) };

		public int GetJoueurScore(int index)
		{
			return joueurs[index].Score;
		}
		
		public int GetJoueurParties(int index)
		{
			return joueurs[index].Parties;
		}
		
		public bool JoueurAvecAvantage(int index)
		{
			return joueurs[index].Avantage;
		}

		public void JeuProcess(int playerIndex)
		{
			var joueur = joueurs[playerIndex];
			if (joueur.IsScoreMax())
			{
				Joueur adversaire = joueurs.Where(adversaireId => adversaireId.Id != joueur.Id).First();
				
				if (joueur.Avantage)
				{
					GagnePartie(joueur, adversaire);
				}
				else if (adversaire.IsScoreMax())
				{
					if (adversaire.Avantage)
						PerdAvantage(adversaire);
					else
						GagneAvantage(joueur);
				}
				else
				{
					GagnePartie(joueur, adversaire);
				}
			}
			else
			{
				joueur.PointGagne();
			}
		}

		private void GagnePartie(Joueur joueur, Joueur adversaire)
		{
			joueur.JeuGagne();
			adversaire.JeuPerdu();
		}

		private void PerdAvantage(Joueur joueur)
		{
			joueur.AvantagePerdu();
		}
		
		private void GagneAvantage(Joueur joueur)
		{
			joueur.AvantageGagne();
		}
	}
}
