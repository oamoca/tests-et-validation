using Tennis;
using Xunit;

namespace TennisTest
{
	public class TennisTest
	{
		private Jeu partie = new Jeu();
		private Joueur joueur = new Joueur(1);

		[Fact]
		public void PremierTourPointGagne()
		{
			int expectedScore = 15;
			joueur.AugmenterScore();
			
			Assert.Equal(expectedScore, joueur.Score);
		}

		[Fact]
		public void DeuxiemeTourPointGagne()
		{
			int expectedScore = 30;
			joueur.AugmenterScore();
			joueur.AugmenterScore();
			
			Assert.Equal(expectedScore, joueur.Score);
		}

		[Fact]
		public void TroisiemeTourPointGagne()
		{
			int expectedScore = 40;
			joueur.AugmenterScore();
			joueur.AugmenterScore();
			joueur.AugmenterScore();
			
			Assert.Equal(expectedScore, joueur.Score);
		}

		[Fact]
		public void QuatriemeTourPointGagne_PartieGagnee()
		{
			int expectedParties = 1;
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);

			Assert.Equal(expectedParties, partie.GetJoueurParties(0));
		}

		[Fact]
		public void ResetScore_PartieGagnee()
		{
			int expectedScore = 0;
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);

			Assert.Equal(expectedScore, partie.GetJoueurScore(0));
		}

		[Fact]
		public void GagneAvantage()
		{
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(1);
			partie.JeuProcess(1);
			partie.JeuProcess(1);
			partie.JeuProcess(0);

			Assert.True(partie.JoueurAvecAvantage(0));
		}

		[Fact]
		public void PerdAvantage()
		{
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(0);
			partie.JeuProcess(1);
			partie.JeuProcess(1);
			partie.JeuProcess(1);
			partie.JeuProcess(1);
			partie.JeuProcess(0);

			Assert.False(partie.JoueurAvecAvantage(1));
		}

	}
}