using Xunit;
using Impots;

namespace ImpotsTest
{
	public class ImpotsTest
	{
		ImpotsRevenu impotsRevenu = new ImpotsRevenu();

		[Theory]
		[InlineData(0, 0)]
		[InlineData(10777, 0)]
		[InlineData(10778, 0.11)]
		[InlineData(27478, 0.11)]
		[InlineData(27479, 0.30)]
		[InlineData(78570, 0.30)]
		[InlineData(78571, 0.41)]
		[InlineData(168994, 0.41)]
		[InlineData(168995, 0.45)]
		[InlineData(200000, 0.45)]
		public void TauxImpots(int revenu, double expectedTaux)
		{
			double tauxImpots = impotsRevenu.TauxImpots(revenu);
			Assert.Equal(expectedTaux, tauxImpots);
		}

		[Theory]
		[InlineData(5000, 0)]
		[InlineData(17000, 684.53)]
		[InlineData(30000, 756.6)]
		[InlineData(80000, 586.3)]
		[InlineData(180000, 4952.7)]
		public void CalculImpots(int revenu, double expectedImpots)
		{
			double calculImpots = impotsRevenu.CalculerImpots(revenu);
			Assert.Equal(expectedImpots, calculImpots);
		}
	}
}