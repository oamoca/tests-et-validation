﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impots
{
	public class ImpotsRevenu
	{
		public double CalculerImpots(double revenu)
		{
			double impots = 0;
			double revenuImposable = 0;
			
			if (revenu < 10777)
			{
				impots = 0;
			}
			if (revenu >= 10778 && revenu <= 27478)
			{
				revenuImposable = revenu - 10777;
				impots = revenuImposable * 0.11;
			}
			else if (revenu > 27479 && revenu <= 78570)
			{
				revenuImposable = revenu - 27478;
				impots = revenuImposable * 0.3;
			}
			else if (revenu > 78571 && revenu <= 168994)
			{
				revenuImposable = revenu - 78570;
				impots = revenuImposable * 0.41;
			}
			else if (revenu > 168994)
			{
				revenuImposable = revenu - 168994;
				impots = revenuImposable * 0.45;
			}
			return impots;
		}

		public double TauxImpots(double revenu)
		{
			double taux = 0;
			if (revenu < 10777)
			{
				taux = 0;
			}
			if (revenu >= 10778 && revenu <= 27478)
			{
				taux = 0.11;
			}
			else if (revenu >= 27479 && revenu <= 78570)
			{
				taux = 0.3;
			}
			else if (revenu >= 78571 && revenu <= 168994)
			{
				taux = 0.41;
			}
			else if (revenu > 168994)
			{
				taux = 0.45;
			}
			return taux;
		}
	}
}
