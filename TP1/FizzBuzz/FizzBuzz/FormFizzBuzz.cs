﻿using FizzBuzzGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Seance2
{
    public partial class FrmLesKatas : Form
    {
		private Generator generator;

		public FrmLesKatas()
        {
			generator = new Generator();
			InitializeComponent();
        }

		private void txtValeur_KeyPress(object sender, KeyPressEventArgs e)
		{
			// Check if the pressed key is a digit or a control character
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void btnValider_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(txtValeur.Text))
			{
				if (Int32.TryParse(txtValeur.Text, out int value))
				{
					if (value < 15 || value > 150)
					{
						MessageBox.Show("Valeur doit être entre 15 et 150", "Error value", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					else
					{
						tbResult.Text = generator.Generate(Int32.Parse(txtValeur.Text));
					}
				}
			}
			else
			{
				MessageBox.Show("Valeur peut pas être null", "Error value", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			
		}
	}
}
