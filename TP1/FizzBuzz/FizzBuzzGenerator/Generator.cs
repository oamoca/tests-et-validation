﻿namespace FizzBuzzGenerator
{
	public class Generator
	{
		private const string _fizzbuzz = "FizzBuzz";
		private const string _fizz = "Fizz";
		private const string _buzz = "Buzz";

		public string Generate(int n)
		{
			string fizzBuzzSentence = string.Empty;
			for (int i = 1; i <= n; i++)
			{
				fizzBuzzSentence += GetFizzBuzzFromInt(i);
				fizzBuzzSentence += " ";
			}
			return fizzBuzzSentence;
		}

		private string GetFizzBuzzFromInt(int i)
		{
			if (NombreMultiple3Et5(i))
				return _fizzbuzz;
			if (NombreMultiple3(i))
				return _fizz;
			if (NombreMultiple5(i))
				return _buzz;
			return i.ToString();
		}

		private bool NombreMultiple3(int nb)
		{
			if (nb == 0)
				return false;
			return nb % 3 == 0;
		}

		private bool NombreMultiple5(int nb)
		{
			if (nb == 0)
				return false;
			return nb % 5 == 0;
		}

		private bool NombreMultiple3Et5(int nb)
		{
			return NombreMultiple3(nb) && NombreMultiple5(nb);
		}
	}
}
